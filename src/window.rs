use gio::FileExt;
use glib::Sender;
use gtk::prelude::*;
use gtk::prelude::*;
use gtk::ResponseType;
use std::path::PathBuf;

use crate::config::{APP_ID, PROFILE};
use crate::window_state;
use glib::WeakRef;

pub struct Window {
    pub widget: WeakRef<gtk::ApplicationWindow>,
    settings: gio::Settings,
    builder: gtk::Builder,
}

impl Window {
    pub fn new() -> Self {
        let settings = gio::Settings::new(APP_ID);

        let builder = gtk::Builder::from_resource("/org/gnome/MetainfoPreview/window.ui");
        get_widget!(builder, gtk::ApplicationWindow, window);

        let window_widget = Window {
            widget: window.downgrade(),
            settings,
            builder,
        };
        window_widget.init();
        window_widget.setup_signals();
        window_widget
    }

    fn init(&self) {
        let window = self.widget.upgrade().unwrap();
        // Devel Profile
        if PROFILE == "Devel" {
            window.get_style_context().add_class("devel");
        }

        // load latest window state
        window_state::load(&window, &self.settings);

        // save window state on delete event
        window.connect_close_request(
            clone!(@weak self.settings as settings => @default-return glib::signal::Inhibit(false), move |window| {
                if let Err(err) = window_state::save(&window, &settings) {
                    log::warn!("Failed to save window state, {}", &err);
                }
                glib::signal::Inhibit(false)
            }),
        );
    }

    pub fn get_window(&self) -> gtk::ApplicationWindow {
        self.widget.upgrade().unwrap()
    }

    pub fn setup_signals(&self) {
        get_widget!(self.builder, gtk::Button, open_button);
        debug!("Setup signal");
        let window = self.get_window();
        open_button.connect_clicked(clone!(@weak window => move |_| {
            let filter = gtk::FileFilter::new();
            filter.set_property("name", &"OPML file".to_string()).expect("Could not set name for file filter");
            filter.add_mime_type("application/xml");
            filter.add_mime_type("text/xml");
            filter.add_mime_type("text/x-opml");
            open(&window, "Select", &[filter], send!("aa".into()));
        }));
    }
}

pub fn open(
    parent: &gtk::ApplicationWindow,
    title: &str,
    filter: &[gtk::FileFilter],
    sender: &Sender<String>,
) {
    let file_chooser = gtk::FileChooserNative::new(
        Some(title),
        Some(parent),
        gtk::FileChooserAction::Open,
        Some("Select"),
        Some("Cancel"),
    );

    for f in filter {
        file_chooser.add_filter(f);
    }

    // let mut f = String::new();
    file_chooser.connect_response(move |file_chooser, resp| {
        debug!("Dialog Response {}", resp);
        if resp == ResponseType::Accept {
        } else {
            send!(sender, "".to_string())
        }
        // if resp == ResponseType::Accept {
        //     if let Some(file) = dialog.get_file() {
        //         debug!("File selected: {:?}", file);
        //         let info = file.query_info("*", gio::FileQueryInfoFlags::empty(), None::<&gio::Cancellable>)
        //                        .expect("Could not get file info");
        //         let filename = info.get_name().expect("Could not get filename; This should never happen").to_owned();
        //         debug!("{:?}",filename);
        //     }
        // }
    });

    file_chooser.show();
    // // let filename = gio::File::from(f);
    // let filename = gio::File::load_contents(None);
}
