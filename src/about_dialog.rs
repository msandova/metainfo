use crate::config;
use gtk::prelude::*;

pub fn show_about_dialog(window: &gtk::ApplicationWindow) {
    let version: String = match config::PROFILE {
        "Devel" => format!(
            "{} \n(Development Commit {})",
            config::VERSION,
            config::COMMIT
        ),
        _ => format!("{}", config::VERSION),
    };

    let dialog = gtk::AboutDialog::new();
    dialog.set_program_name(config::APP_NAME);
    dialog.set_logo_icon_name(Some(config::APP_ID));
    dialog.set_license_type(gtk::License::MitX11);
    dialog.set_website(Some("https://gitlab.gnome.org/msandova/metainfo"));
    dialog.set_version(Some(&version));

    dialog.set_transient_for(Some(window));
    dialog.set_modal(true);

    dialog.set_authors(&["Maximiliano Sandoval"]);
    dialog.set_artists(&["Maximiliano Sandoval"]);

    dialog.show();
}
