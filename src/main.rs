#[macro_use]
extern crate log;
extern crate pretty_env_logger;
#[macro_use]
extern crate glib;

use gettextrs::*;

#[macro_use]
mod utils;

mod about_dialog;
mod application;
#[rustfmt::skip]
mod config;
#[rustfmt::skip]
mod static_resources;
mod metainfo;
mod package_details;
mod package_widgets;
mod window;
mod window_state;

use application::Application;
use config::{GETTEXT_PACKAGE, LOCALEDIR};

fn main() {
    // Initialize logger
    pretty_env_logger::init();

    // Prepare i18n
    setlocale(LocaleCategory::LcAll, "");
    bindtextdomain(GETTEXT_PACKAGE, LOCALEDIR);
    textdomain(GETTEXT_PACKAGE);

    glib::set_application_name(&format!("Metainfo Preview{}", config::NAME_SUFFIX));
    glib::set_prgname(Some("metainfo-preview"));

    gtk::init().expect("Unable to start GTK4");

    static_resources::init().expect("Failed to initialize the resource file.");

    let app = Application::new();
    app.run();
}
