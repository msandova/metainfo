use gio::prelude::*;
use gtk::prelude::*;
use std::env;

use crate::about_dialog::show_about_dialog;

use crate::config;
use crate::window::Window;

#[derive(Debug, Clone)]
pub enum Action {
    ViewSet(View),
    ViewGoBack,
}

pub struct Application {
    app: gtk::Application,
    window: Window,
}

impl Application {
    pub fn new() -> Self {
        let app =
            gtk::Application::new(Some(config::APP_ID), gio::ApplicationFlags::FLAGS_NONE).unwrap();
        let window = Window::new();

        let application = Self { app, window };

        application.setup_widgets();
        application.setup_gactions();
        application.setup_signals();
        application.setup_css();
        application
    }

    fn setup_widgets(&self) {
        let builder = gtk::Builder::from_resource("/org/gnome/MetainfoPreview/shortcuts.ui");
        get_widget!(builder, gtk::ShortcutsWindow, shortcuts);
        self.window
            .widget
            .upgrade()
            .unwrap()
            .set_help_overlay(Some(&shortcuts));
    }

    fn setup_gactions(&self) {
        let window = self.window.widget.upgrade().unwrap();
        // Quit
        action!(
            self.app,
            "quit",
            clone!(@weak self.app as app => move |_, _| {
                app.quit();
            })
        );
        self.app.set_accels_for_action("app.quit", &["<primary>q"]);

        // About
        action!(
            self.app,
            "about",
            clone!(@weak window => move |_, _| {
                show_about_dialog(&window);
            })
        );
        self.app
            .set_accels_for_action("win.show-help-overlay", &["<primary>question"]);
    }

    fn setup_signals(&self) {
        let window = self.window.widget.upgrade().unwrap();
        self.app
            .connect_activate(clone!(@weak window => move |app| {
                window.set_application(Some(app));
                app.add_window(&window);
                window.present();
            }));
    }

    fn setup_css(&self) {
        let p = gtk::CssProvider::new();
        gtk::CssProvider::load_from_resource(&p, "/org/gnome/MetainfoPreview/style.css");
        if let Some(display) = gdk::Display::get_default() {
            gtk::StyleContext::add_provider_for_display(
                &display,
                &p,
                gtk::STYLE_PROVIDER_PRIORITY_APPLICATION,
            );
        }
    }

    pub fn run(&self) {
        info!(
            "Metainfo Preview{} ({})",
            config::NAME_SUFFIX,
            config::APP_ID
        );
        info!("Version: {} ({})", config::VERSION, config::PROFILE);
        info!("Datadir: {}", config::PKGDATADIR);

        let args: Vec<String> = env::args().collect();
        self.app.run(&args);
    }
}
